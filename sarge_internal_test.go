package sarge

import (
	"bytes"
	"testing"
)

type fakeOS struct {
	exitCalled bool
	exitCode   int
	stdout     bytes.Buffer
	stderr     bytes.Buffer
}

func (fos *fakeOS) Exit(code int) {
	fos.exitCalled = true
	fos.exitCode = code
}

func setFakeOS() *fakeOS {
	var osMock fakeOS

	osExit = osMock.Exit
	osStdout = &osMock.stdout
	osStderr = &osMock.stderr

	return &osMock
}

func TestParser_MustExecute(t *testing.T) {
	t.Run("Runs without error if argument parsing has no errors", func(t *testing.T) {
		// Arrange
		type Args struct {
			Foo string
		}

		var args Args

		parser := New(&args)

		setOSArgs(t, "--foo", "bar")

		mockOS := setFakeOS()

		// Act
		parser.MustExecute()

		// Assert
		assertEquals(t, false, mockOS.exitCalled)
	})

	t.Run("Exits and prints error if help requested", func(t *testing.T) {
		// Arrange
		type Args struct {
			Foo string
		}

		var args Args

		parser := New(&args)

		setOSArgs(t, "-h")

		mockOS := setFakeOS()

		// Act
		parser.MustExecute()

		// Assert
		assertEquals(t, true, mockOS.exitCalled)
		assertEquals(t, 0, mockOS.exitCode)
		assertEquals(t, parser.Help(), mockOS.stdout.String())
	})

	t.Run("Exits if execution fails for unknown reason", func(t *testing.T) {
		// Arrange
		type Args struct {
			Foo int
		}

		var args Args

		parser := New(&args)

		setOSArgs(t, "--foo", "not-an-int")

		mockOS := setFakeOS()

		// Act
		parser.MustExecute()

		// Assert
		assertEquals(t, true, mockOS.exitCalled)
		assertEquals(t, 1, mockOS.exitCode)
		assertEquals(t, parser.Help(), mockOS.stdout.String())
		assertContains(t, "error: invalid value for flag --foo: not-an-int", mockOS.stderr.String())
	})

	t.Run("Exits and prints version number if --version specified", func(t *testing.T) {
		// Arrange
		type Args struct {
			Foo int
		}

		var args Args

		parser := New(&args,
			WithVersion("v1.1.1"),
		)

		setOSArgs(t, "--version")

		mockOS := setFakeOS()

		// Act
		parser.MustExecute()

		// Assert
		assertEquals(t, true, mockOS.exitCalled)
		assertEquals(t, 0, mockOS.exitCode)
		assertEquals(t, parser.Version(), mockOS.stdout.String())
	})
}
