# Please note, this library is still experimental, and its API is likely to change

`sarge` is a module to do struct-based configuration parsing. It can read values from command-line arguments, environment variables, or from configuration files. It's an opinionated framework that prioritizes ease-of-use, sane defaults, and few dependencies.

```go
import "gitlab.com/hackandsla.sh/sarge"
```

## Features

- **Simple** - Just define a struct, and you automatically get flags and environment variables defined
- **Type-safe** - All arguments are configured via an functional options API, free from things like struct tags, `interface{}`, and magic
- **(Mostly) Dependency Free** - This library is light-weight, and doesn't require pulling in an excessive number of packages
- **Extensible** - Rather than making assumptions about how your app handles things like config files or remote key-value stores, it gives you the power to layer it into your app's configuration yourself

## Quick Start

Here's a [bare-bones example](examples/simple/main.go) to get started.

```go
func main(){
  // Define our app's configuration struct.
  type Args struct {
    Name string
    Port int
    DB   struct {
      Username string
      Password string
    }
  }

  var args Args

  // Create a new sarge parser.
  p := sarge.New(&args)

  // Execute our command, marshaling values from the environment into our struct.
  p.MustExecute()

  // Use the configuration struct.
  fmt.Printf("Name: %s\n", args.Name)
  fmt.Printf("Port: %d\n", args.Port)
  fmt.Printf("DB.Username: %s\n", args.DB.Username)
  fmt.Printf("DB.Password: %s\n", args.DB.Password)
}
```

Then to use the app:

```bash
# Specify args via command-line arguments
go run main.go --port 9090 --db-username foo --db-password S3cret

# Or specify args via environment variables
PORT=9090 DB_USERNAME=foo DB_PASSWORD=S3cret go run main.go
```

## Configuration

### Help Text

To specify help text on a command, you can use the `WithDescription` when creating a parser:

```go
p := sarge.New(&args,
  sarge.WithDescription("This is a command description"),
)
```

To specify version information on the binary, you can use the `WithVersion` option:

```go
p := sarge.New(&args,
  sarge.WithVersion("v1.1.1"),
)
```

To specify help text on a flag, you can use the `WithHelp` option:

```go
p.String(&args.Name, sarge.WithHelp("The name to use"))
```

See the [full example](./examples/helptext/main.go) for more details.

### Defaults

To specify defaults, just assign values to the struct before marshaling it:

```go
type Args struct {
  Port int
}

// Specify defaults by setting their values in the config struct.
args := Args{
  Port: 8080,
}

p := sarge.New(&args)
```

If a value is specified as an environment variable or command-line argument, it will be overridden. See the [example](examples/defaults/main.go) for more details.

### Custom Values

To use a custom value, you can implement the `sarge.Value` interface.

```go
type UppercaseString struct {
  s string
}

func (us *UppercaseString) UnmarshalText(b []byte) error {
  us.s = strings.ToUpper(string(b))
  return nil
}

func (us *UppercaseString) String() string {
  return us.s
}

var _ sarge.Value = (*UppercaseString)(nil)
```

You can then use it like any other value:

```go
type Args struct {
  Name UppercaseString
}

var args Args

p := sarge.New(&args)
```

See [the full example](examples/customValue/main.go) for more details.

### Custom Marshalers

It's possible to add your own marshalers, which get layered on top of other inputs. This allows retrieving configuration values from custom locations, e.g. configuration files.

First, implement a `sarge.Unmarshaler` like so:

```go
type YAMLConfig struct {
  file string
}

func (cfg *YAMLConfig) Unmarshal(args interface{}) error {
  b, err := os.ReadFile(cfg.file)
  if err != nil {
    if errors.Is(err, os.ErrNotExist) {
      return nil
    }

    return fmt.Errorf("error reading file '%s': %w", cfg.file, err)
  }

  return yaml.Unmarshal(b, args)
}
```

Then add it to the parser:

```go
p := sarge.New(&args,
  sarge.WithUnmarshaler(&YAMLConfig{file: "./config.yaml"}),
)
```

See [the full example](examples/unmarshaler/main.go) for more details

## Motivation

There are already a whole bunch of libraries for [command-line parsing](https://awesome-go.com/#command-line) and [configuration parsing](https://awesome-go.com/#configuration). While I have used Viper and Cobra for many of my projects, I consistently found that it was far too complex for my use cases. The amount of flexibility given by those systems often caused tricky and hard-to-catch errors.

There are several libraries that do struct-based configuration parsing:

- [github.com/jessevdk/go-flags](https://github.com/jessevdk/go-flags)
- [github.com/alexflint/go-arg](https://github.com/alexflint/go-arg)

This module takes the same idea, but makes a couple of improvements on them:

- It uses a statically-typed API to configure the parsing instead of struct tags.
- It enables nested configuration structs with sane (albeit opinionated) defaults.

Most of the ideas for this library originated from the excellent [go-arg](https://github.com/alexflint/go-arg) module.

## License

This project is [MIT Licensed](LICENSE).
