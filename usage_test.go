package sarge_test

import (
	"testing"

	"gitlab.com/hackandsla.sh/sarge"
)

func TestParser_Help(t *testing.T) {
	tests := []struct {
		name   string
		file   string
		parser func() *sarge.Parser
	}{
		{
			name: "Parses a standard argument set",
			file: "testdata/usage_simple.txt",
			parser: func() *sarge.Parser {
				args := struct {
					String  string
					Integer int
				}{}

				p := sarge.New(&args,
					sarge.WithDescription("This is a sample command description."),
				)

				p.String(&args.String,
					sarge.WithShortFlag('s'),
					sarge.WithHelp("This is a string"),
				)

				p.Int(&args.Integer,
					sarge.WithHelp("This is an integer"),
				)

				return p
			},
		},
		{
			name: "Parses an argument set with defaults",
			file: "testdata/usage_defaults.txt",
			parser: func() *sarge.Parser {
				args := struct {
					String  string
					Integer int
				}{}

				p := sarge.New(&args,
					sarge.WithDescription("This is a sample command description."),
				)

				args.String = "foo"
				p.String(&args.String,
					sarge.WithShortFlag('s'),
					sarge.WithHelp("This is a string"),
				)

				args.Integer = 10
				p.Int(&args.Integer,
					sarge.WithHelp("This is an integer"),
				)

				return p
			},
		},
		{
			name: "Parses an argument set with no command description",
			file: "testdata/usage_no_description.txt",
			parser: func() *sarge.Parser {
				args := struct {
					String  string
					Integer int
				}{}

				p := sarge.New(&args)

				p.String(&args.String,
					sarge.WithShortFlag('s'),
					sarge.WithHelp("This is a string"),
				)

				p.Int(&args.Integer,
					sarge.WithHelp("This is an integer"),
				)

				return p
			},
		},
		{
			name: "Includes version flag if version is set",
			file: "testdata/usage_version.txt",
			parser: func() *sarge.Parser {
				args := struct {
					String  string
					Integer int
				}{}

				p := sarge.New(&args,
					sarge.WithDescription("This is a sample command description."),
					sarge.WithVersion("v1.1.1"),
				)

				p.String(&args.String,
					sarge.WithShortFlag('s'),
					sarge.WithHelp("This is a string"),
				)

				p.Int(&args.Integer,
					sarge.WithHelp("This is an integer"),
				)

				return p
			},
		},
	}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			// Arrange
			setOSArgs(t, "--help")

			got := tt.parser().Help()

			assertTextCompare(t, got, tt.file)
		})
	}
}

func TestParser_Version(t *testing.T) {
	// Arrange
	var args struct{}
	parser := sarge.New(&args,
		sarge.WithVersion("v1.1.1"),
	)

	setOSArgs(t)

	// Act
	got := parser.Version()

	// Assert
	assertEquals(t, "progname v1.1.1\n", got)
}
