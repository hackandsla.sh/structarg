
This is a sample command description.

Usage:
  progname [flags]

Flags:
      --integer   This is an integer (default 10)
  -s, --string    This is a string (default foo)
  -h, --help      View this help text
