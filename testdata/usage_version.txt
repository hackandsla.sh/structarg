
This is a sample command description.

Usage:
  progname [flags]

Flags:
      --integer   This is an integer
  -s, --string    This is a string
  -h, --help      View this help text
      --version   Print version information
