package sarge_test

import (
	"testing"
	"time"

	"gitlab.com/hackandsla.sh/sarge"
)

func TestParser_Unmarshal_duration(t *testing.T) {
	// Arrange
	type Args struct {
		Foo time.Duration
	}

	var args Args

	parser := sarge.New(&args)

	setOSArgs(t, "--foo", "5m")

	// Act
	parser.Duration(&args.Foo)
	err := parser.Execute()

	// Assert
	assertNilErr(t, err)
	assertEquals(t, 5*time.Minute, args.Foo)
}

func TestParser_Parser_errorsOnInvalidDuration(t *testing.T) {
	// Arrange
	type Args struct {
		Foo time.Duration
	}

	var args Args

	parser := sarge.New(&args)

	setOSArgs(t, "--foo", "not-a-duration")

	// Act
	err := parser.Execute()

	// Assert
	assertErrIs(t, sarge.ErrInvalidValue, err)
}
