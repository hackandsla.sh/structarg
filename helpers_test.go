package sarge_test

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"reflect"
	"strings"
	"testing"

	"github.com/hexops/gotextdiff"
	"github.com/hexops/gotextdiff/myers"
	"github.com/hexops/gotextdiff/span"
)

const testProgName = "progname"

func setOSArgs(t *testing.T, args ...string) {
	t.Helper()

	oldArgs := os.Args

	os.Args = append([]string{testProgName}, args...)

	t.Cleanup(func() {
		os.Args = oldArgs
	})
}

func assertNilErr(t *testing.T, err error) {
	t.Helper()

	if err != nil {
		t.Errorf("expected nil error, got %v", err)
	}
}

func assertErrIs(t *testing.T, want, got error) {
	t.Helper()

	if !errors.Is(got, want) {
		t.Errorf("got err '%v', want '%v'", got, want)
	}
}

func assertEquals(t *testing.T, want, got interface{}) {
	t.Helper()

	if !reflect.DeepEqual(want, got) {
		t.Errorf("expected '%v', got '%v'", want, got)
	}
}

func assertTextCompare(t *testing.T, got, file string) {
	t.Helper()

	want, err := os.ReadFile(file)
	if err != nil {
		t.Fatalf("couldn't read file '%s': %v", file, err)
	}

	edits := myers.ComputeEdits(span.URIFromPath(file), got, string(want))
	if len(edits) == 0 {
		return
	}

	diff := fmt.Sprint(gotextdiff.ToUnified("got", "want", got, edits))
	t.Error(diff)
}

type customValue struct {
	a string
}

func (v *customValue) UnmarshalText(b []byte) error {
	v.a = strings.ToUpper(string(b))

	return nil
}

func (v *customValue) String() string { return v.a }

type JSONConfig struct {
	file string
}

func (cfg *JSONConfig) Unmarshal(args interface{}) error {
	b, err := os.ReadFile(cfg.file)
	if err != nil {
		return fmt.Errorf("error reading file '%s': %w", cfg.file, err)
	}

	return json.Unmarshal(b, args)
}
