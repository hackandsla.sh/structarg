package sarge

import (
	"strconv"
)

func (p *Parser) Int(i *int, options ...ArgOption) {
	p.Options(i, options...)
}

type intValue struct {
	ptr *int
}

func newIntValue(v interface{}) Value {
	ptr := v.(*int)
	return &intValue{ptr}
}

func (v *intValue) UnmarshalText(b []byte) error {
	i, err := strconv.Atoi(string(b))
	if err != nil {
		return err
	}

	*v.ptr = i

	return nil
}

func (v *intValue) String() string {
	if v.ptr == nil || *v.ptr == 0 {
		return ""
	}

	return strconv.Itoa(*v.ptr)
}
