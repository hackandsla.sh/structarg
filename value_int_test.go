package sarge_test

import (
	"testing"

	"gitlab.com/hackandsla.sh/sarge"
)

func TestParser_Unmarshal_int(t *testing.T) {
	// Assert
	type Args struct {
		Foo int
	}

	var args Args

	parser := sarge.New(&args)

	setOSArgs(t, "--foo", "42")

	// Act
	parser.Int(&args.Foo)
	err := parser.Execute()

	// Assert
	assertNilErr(t, err)
	assertEquals(t, 42, args.Foo)
}
