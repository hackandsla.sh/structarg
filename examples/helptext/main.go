package main

import (
	"fmt"

	"gitlab.com/hackandsla.sh/sarge"
)

func main() {
	type Args struct {
		Name string
		Port int
		DB   struct {
			Username string
			Password string
		}
	}

	var args Args

	p := sarge.New(&args,
		sarge.WithDescription("This is a command description"),
		sarge.WithVersion("v1.1.1"),
	)

	p.String(&args.Name, sarge.WithHelp("The name to use"))
	p.Int(&args.Port, sarge.WithHelp("The port to start the HTTP server on"))
	p.String(&args.DB.Username, sarge.WithHelp("The username to connect to the database with"))
	p.String(&args.DB.Password, sarge.WithHelp("The password to connect to the database with"))

	p.MustExecute()

	fmt.Printf("Name: %s\n", args.Name)
	fmt.Printf("Port: %d\n", args.Port)
	fmt.Printf("DB.Username: %s\n", args.DB.Username)
	fmt.Printf("DB.Password: %s\n", args.DB.Password)
}
