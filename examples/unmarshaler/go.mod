module gitlab.com/hackandsla.sh/sarge/examples/unmarshaler

go 1.17

replace gitlab.com/hackandsla.sh/sarge => ../..

require (
	gitlab.com/hackandsla.sh/sarge v0.0.0-00010101000000-000000000000
	gopkg.in/yaml.v2 v2.4.0
)

require github.com/spf13/pflag v1.0.5 // indirect
