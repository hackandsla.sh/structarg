package main

import (
	"fmt"

	"gitlab.com/hackandsla.sh/sarge"
)

func main() {
	type Args struct {
		Port int
	}

	// Specify defaults by setting their values in the config struct.
	args := Args{
		Port: 8080,
	}

	p := sarge.New(&args)

	p.MustExecute()

	fmt.Printf("Port: %d\n", args.Port)
}
