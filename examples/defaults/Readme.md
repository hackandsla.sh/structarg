# Configuring default values

To run this example, just do

```bash
# Run using the default values
go run main.go

# OVerride the default via command-line argument
go run main.go --port 9090
```
